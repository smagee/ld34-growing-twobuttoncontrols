#include "Tile.h"

Tile::Tile(std::string name, int type, int width, int height, int x, int y, sf::Texture* texture)
{
	this->name = name;
	this->type = type;
	this->width = width;
	this->height = height;
	this->posX = x;
	this->posY = y;
	this->texture = texture;
	this->texX = this->type % (texture->getSize().x / width);
	this->texY = this->type / (texture->getSize().x / width);
	this->tile.setTexture(*this->texture);
	this->tile.setTextureRect(sf::IntRect(texX * this->width, texY * this->height, this->width, this->height));
	this->tile.setPosition(this->posX, this->posY);
}

Tile::~Tile()
{
	//dtor
}

bool Tile::contains(sf::Vector2i mousePos)
{
	return tile.getGlobalBounds().contains(mousePos.x, mousePos.y);
}

void Tile::changeTile()
{
    if(type < 9)
        ++type;
    else
        type = 0;
    texX = type % (texture->getSize().x / width);
    texY = type / (texture->getSize().x / width);
    tile.setTextureRect(sf::IntRect(texX * width, texY * height, width, height));
}

void Tile::changeTile(std::string message)
{
	if(message == "grass")
		type = 0;
	if(message == "dirt")
		type = 1;
	if(message == "path")
		type = 2;
	if(message == "seeded")
		type = 3;
	if(message == "sprouted")
		type = 4;
	if(message == "watered")
		type = 5;
	if(message == "weeds")
		type = 6;
	if(message == "rose")
		type = 7;
	if(message == "tulips")
		type = 8;
	if(message == "hydrangea")
		type = 9;

	texX = type % (texture->getSize().x / width);
	texY = type / (texture->getSize().x / width);
	tile.setTextureRect(sf::IntRect(texX * width, texY * height, width, height));
}

void Tile::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = texture;
	target.draw(tile, states);
}
