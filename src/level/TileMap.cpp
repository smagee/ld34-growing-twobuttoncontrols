#include "TileMap.h"
#include "Patches.h"

TileMap::TileMap()
{
}

TileMap::~TileMap()
{
	//dtor
}

bool TileMap::load(const std::string& tileSet, sf::Vector2u tileSize, const int* tiles, unsigned int width, unsigned int height)
{
	if(!texture.loadFromFile(tileSet))
		return false;

	vertices.setPrimitiveType(sf::Quads);
	vertices.resize(width * height * 4);

	for (int i = 0; i < width; ++i)
		for (int j = 0; j < height; ++j)
		{
			// get the current tile number
			int tileNumber = tiles[i + j * width];

			// find its position in the tileset texture
			int tu = tileNumber % (texture.getSize().x / tileSize.x);
			int tv = tileNumber / (texture.getSize().x / tileSize.x);

			// get a pointer to the current tile's quad
			sf::Vertex* quad = &vertices[(i + j * width) * 4];

			int x = i * tileSize.x;
			int y = j * tileSize.y;
			mapTiles.push_back(new Tile("Tile " + patch::to_string(i + j * width), tileNumber, int(tileSize.x), int(tileSize.y), int(x), int(y), &texture));
		}

	return true;
}

Tile* TileMap::getTile(sf::Vector2i mousePos)
{
	for(auto& tile : mapTiles)
	{
		if (tile->contains(mousePos))
			return tile;
	}

    return nullptr;
}

void TileMap::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	states.texture = &texture;
//	target.draw(vertices, states);
	for(auto& tile : mapTiles)
	{
		target.draw(*tile);
	}
}
