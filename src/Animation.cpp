#include "Animation.h"

Animation::Animation(unsigned int startFrame, unsigned int endFrame, float duration)
{
	this->startFrame = startFrame;
	this->endFrame = endFrame;
	this->duration = duration;
}

Animation::~Animation()
{
	//dtor
}

unsigned int Animation::getLength()
{
	return endFrame - startFrame + 1;
}
