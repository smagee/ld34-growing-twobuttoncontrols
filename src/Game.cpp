#include "Game.h"
#include "State.h"
#include "MenuState.h"

Game::Game()
{
	window.create(sf::VideoMode(640, 640), "Ludum Dare 34 - Growing / Two Button Controls");
	window.setFramerateLimit(60);

	loadFonts();
	loadStylesheets();

	prevState = nullptr;
	currentState = nullptr;
}

Game::~Game()
{
	if (prevState != nullptr)
	{
		prevState->exitState();
		delete prevState;
	}
	if (currentState != nullptr)
	{
		currentState->exitState();
		delete currentState;
	}
}

void Game::startGame()
{
	initializeGame();
	runGameLoop();
}

void Game::initializeGame()
{
	changeState(new MenuState(this));
}

void Game::runGameLoop()
{
	while(window.isOpen())
	{
		currentState->runState();
	}
}

sf::RenderWindow* Game::getWindow()
{
	return &window;
}

void Game::changeState(State* state)
{
	if(prevState != nullptr)
	{
		if(prevState != state)
			prevState->exitState();
	}
	prevState = currentState;

	currentState = state;
	currentState->initState();
}

void Game::loadStylesheets()
{
	stylesheets["Button"] = GuiStyle(
		&fonts.at("MainFont"), 1,
		sf::Color(0xc6, 0xc6, 0xc6), sf::Color(0x94, 0x94, 0x94), sf::Color(0x00, 0x00, 0x00),
		sf::Color(0x61, 0x61, 0x61), sf::Color(0x94, 0x94, 0x94), sf::Color(0x00, 0x00, 0x00)
	);
	stylesheets["Text"] = GuiStyle(
		&fonts.at("MainFont"), 0,
		sf::Color(0x00, 0x00, 0x00, 0x00), sf::Color(0x00, 0x00, 0x00), sf::Color(0xff, 0xff, 0xff),
		sf::Color(0x00, 0x00, 0x00, 0x00), sf::Color(0x00, 0x00, 0x00), sf::Color(0xff, 0x00, 0x00)
	);
}

void Game::loadFonts()
{
	sf::Font font;
	font.loadFromFile("assets/arial.ttf");
	fonts["MainFont"] = font;
}
