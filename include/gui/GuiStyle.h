#ifndef GUISTYLE_H
#define GUISTYLE_H

#include <SFML/Graphics.hpp>

class GuiStyle
{
	public:
		GuiStyle();
		GuiStyle(
			sf::Font* font, float borderSize,
			sf::Color bodyColor, sf::Color borderColor, sf::Color textColor,
			sf::Color bodyHighlightColor, sf::Color borderHighlightColor, sf::Color textHighlightColor);
		virtual ~GuiStyle();

		sf::Color bodyColor;
		sf::Color bodyHighlightColor;
		sf::Color borderColor;
		sf::Color borderHighlightColor;
		sf::Color textColor;
		sf::Color textHighlightColor;
		sf::Font* font;
		float borderSize;
	protected:
	private:
};

#endif // GUISTYLE_H
