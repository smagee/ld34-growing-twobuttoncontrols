#ifndef ANIMATIONHANDLER_H
#define ANIMATIONHANDLER_H

#include <SFML/Graphics.hpp>

#include "Animation.h"

class AnimationHandler
{
	public:
		AnimationHandler();
		AnimationHandler(const sf::IntRect& frameSize);
		virtual ~AnimationHandler();

		void AddAnimation(Animation& animation);

		// update the current frame of animation. dt is the time since the update was last called (i.e the time for one frame to be executed)
		void update(const float dt);

		void changeAnimation(unsigned int animation);

		//current section fo texture that shoudl be displayed
		sf::IntRect bounds;
		// pixel dimensions of each individual frame
		sf::IntRect frameSize;
	protected:
	private:
		std::vector<Animation> animations;

		//current time since the animation loop started
		float t;

		int currentAnimation;
};

#endif // ANIMATIONHANDLER_H
