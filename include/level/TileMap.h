#ifndef TILEMAP_H
#define TILEMAP_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#include <SFML/Graphics.hpp>

#include "Tile.h"

class TileMap : public sf::Drawable, public sf::Transformable
{
	public:
		TileMap();
		virtual ~TileMap();

		bool load(const std::string& tileSet, sf::Vector2u tileSize, const int* tiles, unsigned int width, unsigned int height);
		Tile* getTile(sf::Vector2i mousePos);
	protected:
	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

		sf::VertexArray vertices;
		std::vector<Tile*> mapTiles;
		sf::Texture texture;
};

#endif // TILEMAP_H
