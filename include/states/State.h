#ifndef STATE_H
#define STATE_H

#include "Game.h"

class State
{
	public:
		virtual void initState() = 0;
		virtual void runState() = 0;
		virtual void exitState() = 0;
	protected:
		Game* game;
	private:
};

#endif // STATE_H
